﻿using BookStore.Exceptions;
using BookStore.Models;
using System.Collections.Generic;
using System.Linq;

namespace BookStore.Repository
{
    /// <summary>
    /// Class representing a book storage.
    /// </summary>
    public class BookQuat : IBookQuat
    {
        private static IList<Book> _bookStore = new List<Book>();
        private static int _currentBookID { get; set; } = 0;

        /// <summary>
        /// Creates a new book
        /// </summary>
        /// <param name="book"></param>
        public void Add(Book book)
        {
            var existingBook = _bookStore.FirstOrDefault(b => b.ID == book.ID);

            if(existingBook == null)
            {
                book.ID = ++_currentBookID;
                _bookStore.Add(book);
            }
            else
            {
                throw new BookStorageException("Book already exists.");
            }
        }

        /// <summary>
        /// Deletes a book by id
        /// </summary>
        /// <param name="id"></param>
        public void DeleteBookById(int id)
        {
            var existingBook = _bookStore.FirstOrDefault(b => b.ID == id);

            if (existingBook == null)
            {
                throw new BookStorageException("Book does not exist.");
            }
            else
            {
                _bookStore.Remove(existingBook);
            }
        }

        /// <summary>
        /// Gets all books
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Book> GetAllBooks()
        {
            return _bookStore;
        }

        /// <summary>
        /// Gets a book by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Book GetBookById(int id)
        {
            return _bookStore.FirstOrDefault(b => b.ID == id);
        }

        /// <summary>
        /// Update book
        /// </summary>
        /// <param name="id"></param>
        /// <param name="book"></param>
        public void Update(int id, Book book)
        {
            var existingBook = _bookStore.FirstOrDefault(b => b.ID == id);

            if (existingBook == null)
            {
                throw new BookStorageException("Book does not exist.");
            }
            else
            {
                book.ID = existingBook.ID; // make sure you are replacing the same object==> ID will remain same
                _bookStore.Remove(existingBook); // delete old one -- to mimic replacing it.
                _bookStore.Add(book);
            }
        }

        /// <summary>
        /// Clears the store
        /// </summary>
        public void Clear()
        {
            _bookStore?.Clear();
        }
    }
}
