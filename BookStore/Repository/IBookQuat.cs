﻿using BookStore.Models;
using System.Collections.Generic;

namespace BookStore.Repository
{
    public interface IBookQuat
    {
        void Add(Book book);
        void Update(int id, Book book);
        Book GetBookById(int id);
        IEnumerable<Book> GetAllBooks();
        void DeleteBookById(int id);

        void Clear();
    }
}
