﻿using System;

namespace BookStore.Exceptions
{
    public class BookStorageException : Exception
    {
        public BookStorageException(string message) : base(message)
        {
        }
    }
}
