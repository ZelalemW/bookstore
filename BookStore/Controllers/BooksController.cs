﻿using BookStore.Models;
using BookStore.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BookStore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBookQuat _bookQuat;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bookQuat"></param>
        public BooksController(IBookQuat bookQuat)
        {
            _bookQuat = bookQuat;
        }
        
        /// <summary>
        /// Gets all books
        /// </summary>
        /// <returns>list of book</returns>
        [HttpGet]
        public ActionResult<IEnumerable<Book>> GetBooks()
        {
            var books = _bookQuat.GetAllBooks();

            return Ok(books);
        }

        /// <summary>
        /// Get a book by its id
        /// </summary>
        /// <param name="id">book id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<Book> GetBook(int id)
        {
            var book = _bookQuat.GetBookById(id);

            if(book == null)
            {
                return NotFound();
            }

            return Ok(book);
        }

        /// <summary>
        /// Creates a book
        /// </summary>
        /// <param name="book">a book to be saved</param>
        [HttpPost]
        public void Post([FromBody] Book book)
        {
            var existingBook = _bookQuat.GetBookById(book.ID);

            if(existingBook == null)
            {
                _bookQuat.Add(book);
                CreatedAtAction(nameof(GetBook), new { id = book.ID });
            }
            else
            {
                Conflict();
            }
        }

        /// <summary>
        /// Updates an existing book.
        /// </summary>
        /// <param name="id">id of the book to be updated</param>
        /// <param name="book">new details of the book to be replaced by</param>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Book book)
        {
            var existingBook = _bookQuat.GetBookById(id);

            if (existingBook == null)
            {
                NotFound();
            }
            else
            {
                _bookQuat.Update(id, book);
            }
        }

        /// <summary>
        /// Deletes a book by id
        /// </summary>
        /// <param name="id">book id</param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var existingBook = _bookQuat.GetBookById(id);

            if (existingBook == null)
            {
                NotFound();
            }
            else
            {
                _bookQuat.DeleteBookById(id);
            }
        }
    }
}