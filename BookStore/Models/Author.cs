﻿namespace BookStore.Models
{
    public class Author : BaseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Honorefic { get; set; }
    }
}