﻿using System.Collections.Generic;

namespace BookStore.Models
{

    public class Book : BaseModel
    {
        public string ISBN { get; set; }
        public string Title { get; set; }
        public float Price { get; set; }
        public IEnumerable<Author> Authors { get; set; }
        public Publisher Publisher { get; set; }
    }
}
