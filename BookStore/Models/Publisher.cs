﻿namespace BookStore.Models
{
    public class Publisher : BaseModel
    {
        public string Name { get; set; }
        public Address Address { get; set; }
    }
}