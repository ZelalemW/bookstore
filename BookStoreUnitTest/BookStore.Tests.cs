using BookStore.Models;
using BookStore.Repository;
using System;
using System.Linq;
using Xunit;

namespace BookStore.Tests
{
    public class BookStoreTests : IDisposable
    {
        private BookQuat _bookStore;

        public BookStoreTests()
        {
            _bookStore = new BookQuat();
        }

        public void Dispose()
        {
            _bookStore.Clear();
            _bookStore = null;
        }

        [Fact]
        public void Add_book_should_persist()
        {
            _bookStore.Add(new Book());

            var booksCount = _bookStore.GetAllBooks().ToList().Count;

            Assert.Equal(1, booksCount);
        }

        [Fact]
        public void Getting_an_existing_book_should_return_correct_book()
        {
            var book = new Book { ID = 1, Title = "Software Architecture" };
            _bookStore.Add(book);

            var result = _bookStore.GetBookById(book.ID);

            Assert.Equal(book.Title, result.Title);
        }
    }
}
